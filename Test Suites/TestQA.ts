<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TestQA</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f7a4af40-0d77-4f18-968c-8720d7f8ab08</testSuiteGuid>
   <testCaseLink>
      <guid>a8a991de-65ac-4a64-a544-d60431cb07e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Get List User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fa9c0c6-9781-4dc0-ae25-2b1baa90e33d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Get Single User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af7780ae-b820-4dee-961f-4dbc2aa74108</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Update</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d70f8a6c-cff8-44c9-afe9-7164e637acf7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
